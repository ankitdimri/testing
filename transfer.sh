#!/bin/bash

inp=$1
outp=$2

while read line
do
    echo "$line" >> "$outp"
done < "$inp"
